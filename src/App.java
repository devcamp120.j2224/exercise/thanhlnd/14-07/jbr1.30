public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Thanhlnd");

        Employee person = new Employee(1, "Thanh", "Le", 8000000);
        Employee person1 = new Employee(2, "Ngu", "Buc", 7500000);

        System.out.println("Nhân viên 1:");
        System.out.println(person.toString());
        System.out.println("Fullname : " + person.getName());
        System.out.println("Salary 1 year : " + person.getAnnualSalary());
        System.out.println("Raise salary : " + person.raiseSalary(10));

        System.out.println("Nhân viên 2 :");
        System.out.println(person1.toString());
        System.out.println("Fullname : " + person1.getName());
        System.out.println("Salary 1 year : " + person1.getAnnualSalary());
        System.out.println("Raise salary : " + person1.raiseSalary(20));
    }
}

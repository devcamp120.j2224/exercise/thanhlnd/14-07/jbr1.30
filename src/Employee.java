public class Employee {
    int id;
    String firstName;
    String lastName;
    int salary;

    public Employee(int id, String firstName, String lastName, int salary) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getName() {
        return this.firstName + this.lastName;
    }
    
    //Phương thức tính lương 1 năm
    public int getAnnualSalary() {
        return this.salary * 12 ;
    }

    //Phương thức tính tăng lương
    public float raiseSalary(float percent) {
        float raiseSalary = (this.salary) * ((percent / 100)+1);
        return raiseSalary;
    }

    //trả về to string
    @Override
    public String toString() {
        return "Employee [id= " + id + ", firstName= " + firstName + ", lastName=" + lastName + ", salary=" + salary
                + "]";
    }
}
